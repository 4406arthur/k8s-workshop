# Containerize WhitePaper


# Background
In container's world, you must realize its a linux process, instead of a VM.

>  I suggest you only attach what you really run
  
Image base on alpine linux is preferable intead of CentOS, Ubuntu

> purse image size as little as possibe

The alpine linux is the most light linux OS, very suitable with container

> Remember container is a Process



# Immutable Infrastructure

    Container is kind of server templateing

Server templating is a key component of the shift to immutable infrastructure.

> once you’ve deployed a server, you never make 
> 
> changes to it again. If you need to update something 
> (e.g., deploy a new version of your code),
> 
> you create a new image from your server template 
> 
> and you deploy it . 
> 
> Since servers never change, it’s a lot easier to reason about what’s deployed


    how about mutable infrastructure ?
    
> Configuration management tools such as Ansible,
>
> Puppet, typically default to a mutable infrastructure paradigm.
> 
> For example, if you tell Ansible to install a new version of OpenSSL,
> 
> it’ll run the software update on your existing servers
> 
> and the changes will happen in place. 
> 
> Over time, as you apply more and more updates,
> 
> each server builds up a unique history of changes.
> 
> As a result, each server becomes slightly different than all the others, 
>
> leading to subtle configuration bugs that are difficult to diagnose and reproduce.
>
> most important is that some changes may work just fine on a test server, 
> 
> but that same change may behave differently on a production server 
> 
> because the production server has accumulated months of changes 
> 
> that aren’t reflected in the test environment


# Image

you can regard image as infrastructure of container

In OOP paradigm, Class like image, Obejct like container.


## How to build Image

- dockerfile inherit model

- dockerfile multi-stage (after docekr17.05.0-ce-rc1)


## Layer Dockerfile

    this part is guide for dockerfile inherit model

In my experience, your container need a Dockerfile 

which base on lean infra image, including all system runtime.

The successor Dockerfile only deal with your application layer component(sourcecode...).

Separate concerns between the env configuration and Application.

Cons:

- faster image building time

- prevent from changing runtime ENV, more stable.


 
 
above link is my recommendates posts

summarize how to write good Dockerfile: 
- [best-practices-for-working-with-dockerfiles](https://medium.com/@nagarwal/best-practices-for-working-with-dockerfiles-fb2d22b78186)

- [how-to-write-excellent-dockerfiles](https://rock-it.pl/how-to-write-excellent-dockerfiles/?utm_campaign=CodeTengu&utm_medium=web&utm_source=CodeTengu_87)

above is sTune (php service) Dockerfile


```Dockerfile
#base on lean infra image
FROM  arthurma/nginx-php-fpm:php71
MAINTAINER ArthurMa <arthurma@loftechs.com>

#set codebase
COPY src/ /src
ADD config/vendor.tar.gz /src

#ngnix config 
COPY config/default.conf /etc/nginx/sites-enabled


ENTRYPOINT [ "/start.sh" ]

```


## Mix with Ansible

When you found create a suitable lean infra image is hard, time consuming.

>  It's time to use CM tools, like ansible.

Resue such situation when we already have playbook.

here is great demo from William Yeh

[halfblood-docker](http://school.soft-arch.net/blog/247026/halfblood-docker)

## dockerfile multi-stage

    It integrates all you build flow in one Dockerfile like pipeline
![](https://i.imgur.com/dR0fQM1.png)

The multi-stage build allows using multiple FROM 

commands in the same Dockerfile.

The last FROM command produces the final Docker image,

all other images are intermediate images (only 

final Docker image is produced, but all layers are cached).

great demo from appleboy [clickme](https://blog.wu-boy.com/2017/04/build-minimal-docker-container-using-multi-stage-for-go-app/)



# Best Practice



[Docker and 12 factor apps are a killer combo and offer a peek into the future of application design and deployment](https://medium.com/@kelseyhightower/12-fractured-apps-1080c73d481c)





