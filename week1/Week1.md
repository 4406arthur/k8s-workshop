Week1
=========

    mentor: ArthurMa

outline
-------
- Kubespray
- TLS X.509 Generate
- Kubelet staic pods
- AA
    - Authenticate
    - Authorize
- Lab01
    - How to resolve/debug connectivity issue ?
    - How to create a user account
    - How to use RBAC
- Related Reference

## kubespray

    complete Doc support, and activity community, the most important is we know playbook

deploy new k8s cluster main flow

```
    role: kubernetes/preinstall
    role: docker
    role: etcd
    role: kubernetes/secrets
    role: kubernetes/node
    role: kubernetes/master
    role: kubernetes/client
    role: kubernetes-apps/cluster_roles
    role: network_plugin
    role: kubernetes-apps
```

To understand the deploy process,
you can go through playbook with above order. 


### ***especially***
- kubernetes/preinstall 
- kubernetes/node
- kubernetes/master
- network_plugin
- kubernetes-apps

## Kubelet static pods

    deploy Kube component as pods instead of normal daemon process
    
- kube-apiserver
- kube-controller-manager
- kube-proxy
- kube-scheduler

Static pods are managed directly by kubelet daemon on a specific node, without the API server observing it. It does not have an associated replication controller, and kubelet daemon itself watches it and restarts it when it crashes. There is no health check. Static pods are always bound to one kubelet daemon and always run on the same node with it.

Kubelet automatically creates so-called mirror pod on the Kubernetes API server for each static pod, so the pods are visible there, but they cannot be controlled from the API server [check-here](https://kubernetes.io/docs/tasks/administer-cluster/static-pod/)



## TLS X.509 Gen

    Kube component certifacation gen process on kubespray
    
**Reference from kubespray: role/kubernetes/secrets/tasks/gen_certs_script.yaml**

```yaml
- name: Gen_certs | copy certs generation script
  copy:
    src: "make-ssl.sh"
    dest: "{{ kube_script_dir }}/make-ssl.sh"
    mode: 0700
  run_once: yes
  delegate_to: "{{groups['kube-master'][0]}}"
  when: gen_certs|default(false)
  
- name: Gen_certs | run cert generation script
  command: "{{ kube_script_dir }}/make-ssl.sh -f {{ kube_config_dir }}/openssl.conf -d {{ kube_cert_dir }}"
  environment:
    - MASTERS: "{% for m in groups['kube-master'] %}
                  {% if gen_master_certs|default(false) %}
                    {{ m }}
                  {% endif %}
                {% endfor %}"
    - HOSTS: "{% for h in groups['k8s-cluster'] %}
                {% if gen_node_certs[h]|default(true) %}
                    {{ h }}
                {% endif %}
              {% endfor %}"
  run_once: yes
  delegate_to: "{{groups['kube-master'][0]}}"
  when: gen_certs|default(false)
  notify: set secret_changed
```

## AA

## Authenticate

### X509 Client Certs

Client certificate authentication is enabled by passing the --client-ca-file=SOMEFILE option to API server. The referenced file must contain one or more certificates authorities to use to validate client certificates presented to the API server. If a client certificate is presented and verified, the common name of the subject is used as the user name for the request.

For example, using the openssl command line tool to generate a certificate signing request:

```
openssl req -new -key jbeda.pem -out jbeda-csr.pem -subj "/CN=jbeda/O=app1/O=app2"
```

### Static Token File

The API server reads bearer tokens from a file when given the --token-auth-file=SOMEFILE option on the command line. Currently, tokens last indefinitely, and the token list cannot be changed without restarting API server.

The token file is a csv file with a minimum of 3 columns: token, user name, user uid, followed by optional group names. Note, if you have more than one group the column must be double quoted e.g.

```
token,user,uid,"group1,group2,group3"
```

When using bearer token authentication from an http client, the API server expects an Authorization header with a value of Bearer THETOKEN

### Static Password File

Basic authentication is enabled by passing the --basic-auth-file=SOMEFILE option to API server. Currently, the basic auth credentials last indefinitely, and the password cannot be changed without restarting API server.

The basic auth file is a csv file with a minimum of 3 columns: password, user name, user id. followed by optional group names. Note, if you have more than one group the column must be double quoted e.g.

```
password,user,uid,"group1,group2,group3"
```

When using basic authentication from an http client, the API server expects an Authorization header with a value of Basic BASE64ENCODED(USER:PASSWORD)
[ref-from-offical-doc](https://kubernetes.io/docs/admin/authentication/)

### Authorize
### RBAC

After kuberntes v1.6 RBAC have been recommendend to be default.
kubespray deploy also enable this feature in kube-api argument by default

lots of sample [ref-from-offical-doc](https://kubernetes.io/docs/admin/authorization/rbac/)


## LAB-01

Leverage kubespray to deploy 3 node K8s on GCP VM with calico CNI Plugin.

with dashboard, heapster, netchecker, guestbook-app.

- deploy guestbook-app on default namespace use a non system:masters group user
    - use X509 crt or Bearer token.

    
***related .yaml and kubespray inventory setting in workshop repo: week1/***


## Reference

Kubernetes The Hard Way is optimized for learning, which means taking the long route to ensure you understand each task required to bootstrap a Kubernetes cluster.
https://github.com/kelseyhightower/kubernetes-the-hard-way/tree/master/docs

You will deploy an application using a static pod, test it, then upgrade the application.
https://github.com/kelseyhightower/standalone-kubelet-tutorial
