# Deploy Kubernetes
This doc provide step to setup k8s cluster provision.

more advance topic could ref [hobby-kube/guide](https://github.com/hobby-kube/guide)


# Type of Solutions

## Bare metal
    Can support both VM and bare-metal.

method:

- [matchbox](https://github.com/coreos/matchbox) maintains by coreos, intro Ref [clickme](https://www.slideshare.net/secret/F4yyyXeGUYbMl9)
- build from [Scratch](https://kubernetes.io/docs/getting-started-guides/scratch/)
- [kubeadm](https://github.com/luxas/kubeadm-workshop) (recommend) like the way of docker-swarm
- [kubespray](https://github.com/kubernetes-incubator/kubespray) (recommend) ansible-playbook solution


## AWS
     there are all turn key solution

mathod:

- [kube-aws](https://github.com/rimusz/kube-gce/blob/master/README.md) maintains by coreOS.

- [kops](https://github.com/kubernetes/kops/blob/master/docs/aws.md) maintains by kubernetes




## Tectonic
CoreOS host solution for provisioning k8s on your machines.

    including AWS, bare-metal

free up to 10 nodes, but we can learn how [Tectonic](https://coreos.com/tectonic/docs/latest/) solve provision k8s problem
It depends on their opensource stack.


## Rancher
a container ecosystem management plateform opensource.


## case study 
Object:
 
- Rancher
- kargo



# Rancher usage


In Rancher ecosystem, It will setup lot of Rancher services 

(etc. cattle, websocket proxy, go machine service and a mysql database)

and orchestrating k8s essential components.

all services will be encapulated into containers.

therefore, its easy to setup,

but its need lots of extra services for support rancher,

the resource cost is higher than other k8s cluster.



> Here is Rancher Lab offical DEMO

<iframe width="560" height="315" src="https://www.youtube.com/embed/ppY9cqTvBVE" frameborder="0" allowfullscreen></iframe>


# kubespray usage
    
    DEMO-ENV: 
    - Vagrant 1.9.1
    - Virtualbox 5.1.20
         

In this section, I will demo how to use Kargo deploy kubernetes.

Requirements:

- Ansible v2.3 (or newer) and python-netaddr is installed on the machine that will run Ansible commands
- Jinja 2.9 (or newer) is required to run the Ansible Playbooks
- The target servers must have access to the Internet in order to pull docker images.
- The target servers are configured to allow IPv4 forwarding.
- Your ssh key must be copied to all the servers part of your inventory.
- The firewalls are not managed, you'll need to implement your own rules the way you used to. in order to avoid any issue during deployment you should disable your firewall.


## Prerequirement

clone kubespray repo, open your terminal.

```shell

git clone https://github.com/kubernetes-incubator/kubespray.git
```

then, change the workdir into kubespray repo.


I suggest use python virtualenv to setup a dependency environment.

```shell=bash
#/bin/bash

virtualenv .env
source .env/bin/activate

#will give you clean python2.7 env with essential dev tools

pip install -r requirements.txt

#done
```

## Config

trace the Vagrantfile, determin which OS you want.

But in this demo I will use Ubuntu.

    Ubuntu has always been a first class citizen in the Kubernetes ecosystem


In default its a HA k8s environment.

k8s-HA:

- etcd cluster (O)
- Multi Master node (O)


Kargo support above OS:

- Container Linux by CoreOS
- Debian Jessie
- Ubuntu 16.04
- CentOS/RHEL 7


> free to trace the Vagrantfile, if you want to know the infra configuration.
>
> It's Ruby language, easy to read.

Vagrantfile
![](https://i.imgur.com/r2SQy2n.png)


I suggest you modify var`bootstrap_os` in `group_vars/all.yml`

the bootstrap_os variable is hand-set because it's required to determine what 

OS we're installing python on so that ansible can gather facts about the host

![](https://i.imgur.com/70Bjdeq.png)



## Provisioning

ready to provision.

since the Vagrantfile will excute playbook,

We only need one command.

> notice that, ensure your compute resource(RAM space) enough 
> 
> default will run 3 VM on your computer.


```shell
vagrant up
```

if some issue interrupts the process, you need run playbook manually

```shell
ansible-playbook -i inventory/vagrant_ansible_inventory -b cluster.yml
```

finally, if everything goes expectedly.
![](https://i.imgur.com/3zbCfv2.png)


## Check it

If you want connect into vagrant's k8s env

you need setup your local kubectl config

since Kargo did not generate ~/.kube/config file

https://github.com/kubernetes-incubator/kubespray/issues/257,

follow the issue, you will know that dump ca.pem,admin.pem,admin-key.pem

and create kubeconfig by 

```
kubectl config set-cluster
kubectl config set-credentials
kubectl config set-context
```

then you can use kubectl locally connect into this demo k8s env.

or just into master node.


```shell
#/bin/bash

#enter the node 01
vagrant ssh k8s-01

#show cluster-info
kubectl cluster-info

#you will see 3 nodes
kubectl get nodes


#check component status
kubectl get cs


```






















