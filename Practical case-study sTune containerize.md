# Practical case-study sTune containerize
In this section, I will share how sTune application leverage containerize

## Project structure
![](https://i.imgur.com/LsIWjTZ.png)

## How to run sTune project


#### Before Container

> In the tradition development enviroment,
>
> we need setup our Web Server(nginx, apache)...
>
> since It's a PHP Project, We also need setup PHP runtime.
>
> Sound not quit difficult, take look at this guide 
>
> How to Setup PHP development ENV..[click](https://getgrav.org/blog/macos-sierra-apache-multiple-php-versions)
>
>maybe take 10-15 minutes.

#### After Container

> You need install container runtime(docker)
> 
> You need install docker-compose
>
> Done.

with one CMD

```
docker-compose up -d
```

Now, I Can fix bug, develope new feature.

### What's docker-compose

    With Compose, you use a Compose file to configure your application’s services.
    Then, using a single command,
    you create and start all the services from your configuration

here is sTune docker-compose.yml

```yaml
version: '2'
services:
  mongo:
     image: mongo:latest
     ports:
     - "27017:27017"
     volumes:
     - ./test-env/mongo/data:/data/db
     restart:
       always
  redis:
     image: redis
     ports:
     - "6379:6379"
     restart:
       always
  mysql:
     image: mysql
     ports:
     - "3306:3306"
     volumes:
     - ./test-env/mysql/schema:/docker-entrypoint-initdb.d:ro
     restart:
       always
     environment:
       MYSQL_ROOT_PASSWORD: "root"
  stune-api-server:
     # for development env
     image: arthurma/nginx-php-fpm:latest
     ports:
     - "8080:80"
     volumes:
     - ./src:/src
     - ./config/default.conf:/etc/nginx/sites-enabled/default.conf
     - ./config/credentials:/run/secrets/aws_credentials
     depends_on:
     - mongo
     - redis
     - mysql
     restart:
       always

```
sTune depends on 3 data-layer service, mongo, redis, mysql.

The services defined in the docker-compose.yml,

will attach same network.

![](https://i.imgur.com/BkIywQI.png)

Let's see the metadata inside.

```shell=bash
docker network inspect scokeslim_default
```

```json
[
    {
        "Name": "scokeslim_default",
        "Id": "931ed92a652e29dccf133447b553ae20771a8a50af19236afe0e66ad6e0ae79f",
        "Created": "2017-05-09T09:00:42.999583821Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "Containers": {
            "1e9af317e1a3097a3ef2de4fbc17083b8356e7bacaae371bb108a8f76fde0f79": {
                "Name": "scokeslim_stune-api-server_1",
                "EndpointID": "850dad7599b1f10bc365d96ff94d41c689d0fd8f98d4aaf294bffa2a02bb71f7",
                "MacAddress": "02:42:ac:12:00:05",
                "IPv4Address": "172.18.0.5/16",
                "IPv6Address": ""
            },
            "c5656b059500132efbdc8bf3db660620b59402902ae9c3734dd7bc6fac92426c": {
                "Name": "scokeslim_redis_1",
                "EndpointID": "7e79118de03832a1ba53882e7257e11c845e4fdeefffd9fa12be0f475698632f",
                "MacAddress": "02:42:ac:12:00:02",
                "IPv4Address": "172.18.0.2/16",
                "IPv6Address": ""
            },
            "ddb698505eded13196f65b9d675fcb39e33925dc8b12a445f1a2509ca8213042": {
                "Name": "scokeslim_mysql_1",
                "EndpointID": "92f4f24c37ff7fff088ef0e366e4aaca2db1115e8b83d808561e120d21439da2",
                "MacAddress": "02:42:ac:12:00:03",
                "IPv4Address": "172.18.0.3/16",
                "IPv6Address": ""
            },
            "e9ea543e0bf4dd5ca0f82bb8ebe62f66c54915b3c9687e24f45b39d510868b01": {
                "Name": "scokeslim_mongo_1",
                "EndpointID": "e7ec23e45b682f11d817fe209b884291da4e982bae7830b4248f21de3f8e675f",
                "MacAddress": "02:42:ac:12:00:04",
                "IPv4Address": "172.18.0.4/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
```

You will see all my services all attach this network with same subnet.

    Service is discoverable from each other!

Demo docker-compose service discovery
![](https://i.imgur.com/iKLUuPV.png)


Its use a nameserver 127.0.0.11.

you can think you have a dns server on localhost build by docker.


### How do you change the code ?
    
    Are u kidding me ? 
    Enter the container edit code with vim?
    definitely not, Its done by volume

A data volume is a specially-designated directory within one or more containers 

that bypasses the Union File System. 

Data volumes provide several useful features for persistent or shared data:

- Volumes are initialized when a container is created. If the container’s base image contains data at the specified mount point, that existing data is copied into the new volume upon volume initialization.
 
- Data volumes can be shared and reused among containers.
 
- Changes to a data volume are made directly.
 
- Changes to a data volume will not be included when you update an image.
 
- Data volumes persist even if the container itself is deleted.


### How can you call sTune API ?
    
    Enter the continer and use curl ?
    definitely not, its have port forwarding.

In my docker-compose.yml

```yaml
stune-api-server:
  # for development env
  image: arthurma/nginx-php-fpm:latest
  ports:
  - "8080:80"

```

the ports, 8080:80 means on host request with 8080 port 

will forwarding into service stune-api-server:80.


## CI/CD in sTune 
    
    short our software integration, release cycle.
    
    automation if possible! 

Drone is my CI/CD plateform, I am not going to explain here.

I have another chapter for Drone.

what's my workflow ?

Its all definded in .drone.yml

```yaml
pipeline:
  build:
    image: tmaier/docker-compose
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    commands:
      - docker-compose -p scoke -f .drone-compose.yml up --build -d 
  mongoInit:
    image: docker:latest
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    commands:
      - docker exec scoke_mongo_1 
          sleep 5
      - docker exec scoke_mongo_1
          mongoimport
          --db stune
          --collection preference
          --file /schema/preference.json
  test:
    image: docker:latest
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    commands:
      - docker exec scoke_jmeter_1
          sleep 5
      - docker exec scoke_jmeter_1
          /bin/bash -c /home/test.sh
      - docker rm -f $(docker ps -aq --filter="name=scoke")
  cleanup:
    image: docker:latest
    when:
      status: failure
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    commands:
      - docker rm -f $(docker ps -aq --filter="name=scoke")
  #push image to your registry.
  publish:
    image: plugins/docker
    when:
      status: success
      event: tag
    secrets: [ DOCKER_USERNAME,DOCKER_PASSWORD ]  
    dockerfile: Dockerfile
    repo: arthurma/scoke
    tags: ${DRONE_TAG}
  #for casting.
  notify:
    image: plugins/slack
    webhook: https://hooks.slack.com/services/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
    channel: stune
    when:
      status: [ success, failure ]
  #deploy on GCP
  deploy:
     image: quay.io/honestbee/drone-kubernetes
     when:
        status: success
        event: tag
     secrets: [ KUBERNETES_SERVER, KUBERNETES_TOKEN, KUBERNETES_CERT ]
     namespace: default
     deployment: scoke-api-server
     repo: arthurma/scoke
     container: scoke
     tag: ${DRONE_TAG}

branches: master

```
Its case by case, my workflow maybe not suit for you.

you can reference it.

### build phase

I use docker-compose to setup whole service need env.

mongo-init is kind of build phase, mock the data I need.

all service set already

![](https://i.imgur.com/DllI2I6.png)


### test phase 

use a jmeter client to run all API test.

you can see in my drone config have syntax when,

when to trigger.
![](https://i.imgur.com/qEDVhZZ.png)


>if one API test fail, the status will be failure

![](https://i.imgur.com/TW3iQgb.png)


    
### publish phase

when tag event and status success,

I will publish a new image into dockerhub with git tag as my image tag.

![](https://i.imgur.com/KWFpDYm.png)

    
    In CI's point of view, publish phase is the end.
    
    I have release a new version.
    
    depends on your test granularity, your testcase is very huge and great
    
    maybe you suit for doing CD.
![](https://i.imgur.com/fnhJ2v0.png)



### notify phase
    
    active send a IM
    to know you release is success or fail
    I choose use slack plateform to notify
     
![](https://i.imgur.com/CHmU2pe.png)



### Deliver Phase


automation update with new image on taget server.




## deploy your service on kubernetes
    
    In the section I will demo
    How amazing rolling update in k8s
    
Prerequirement:
    
    very important resource type in k8s

- Pods
- ReplicaSet
- Deployment
    
    

kubectl is a client tool for kubernetes,

you need install it firstly.

then setup your kubectl config

default kubectl will load $HOME/.kube/config

if you want to play local you can install minikube.

its a 1 node k8s.

after you setup kubectl

you can try with 

```shell=bash
kubectl get nodes
```

you will see all your nodes in k8s cluster

![](https://i.imgur.com/IxmVged.png)


Lets do rolling update
> Rolling updates allow Deployments' update to take place with
> zero downtime by incrementally updating Pods instances with new ones

here is sTune deployment defined

```yaml

#scoke-deploy.yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: scoke-api-server
spec:
  replicas: 3
  #custom our strategy
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 2
  minReadySeconds: 6
  template:
    metadata:
      labels:
        api: scoke
    spec:
      containers:
        - name: scoke
          image: index.docker.io/arthurma/scoke:latest
          resources:
            requests:
              cpu: 100m
              memory: 100Mi
          volumeMounts:
            - name: secret-vol
              readOnly: true
              mountPath: /run/secrets/
      volumes:
        - name: secret-vol
          secret:
            secretName: scoke-config
      imagePullSecrets:
        - name: myregistrykey

```

### step.1

based on this configuration, create a resource on k8s.

![](https://i.imgur.com/sJe8UcJ.png)

You will see 3 type of resource exists on k8s

![](https://i.imgur.com/5sF75NZ.png)

![](https://i.imgur.com/0MjN4Fn.png)

![](https://i.imgur.com/qg65Jiz.png)

### step.2

after releasing a new version sTune.

We need replace our old pods.

![](https://i.imgur.com/estNIIu.png)

you have another way to rolling update.

we can directly modify our configuration file.

then

```
kubectl replace -f scoke-deploy.yaml
```

## How to rolling back

if you found latest release not stable expectedly.

We can check our rolling history

![](https://i.imgur.com/Ecfk4Lh.png)

Maybe you notice it , why revision 1 doesnt have info..

since first time I create resource with 

```
kubectl apply -f scoke-deploy
```

I miss the --record, its good to trace, remember append --record.

Let rolling back to revision 1

```
kubectl rollout undo deploy/scoke-api-server --to-revision=1
```
In rolling back action, its very fast , since each revision have a 

ReplicaSet save in k8s.
















































